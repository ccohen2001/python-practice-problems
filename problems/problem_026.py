# Complete the calculate_grade function which accepts
# a list of numerical scores each between 0 and 100.
#
# Based on the average of the scores, the function
# returns
#   * An "A" for an average greater than or equal to 90
#   * A "B" for an average greater than or equal to 80
#     and less than 90
#   * A "C" for an average greater than or equal to 70
#     and less than 80
#   * A "D" for an average greater than or equal to 60
#     and less than 70
#   * An "F" for any other average

def calculate_grade(values):
    grade_avg = 0
    for i in range(len(values)):
        grade_avg += values[i]
    grade_avg = grade_avg / len(values)

    if grade_avg >= 90:
        return "A"
    elif grade_avg >= 80:
        return "B"
    elif grade_avg >= 70:
        return "C"
    elif grade_avg >= 60:
        return "D"
    else:
        return "F"


print(calculate_grade([45, 43, 26]))
